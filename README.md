# Catálogo de Produtos #

## product-ms

Este microserviço contém as seguintes funcionalidades:

- Operações CRUD para determinado produto: criar, visualizar, alterar e excluir.

- Visualizar a lista de produtos atuais disponíveis.

- Realizar a busca de produtos filtrando por name, description e price.

## Especificação de Requisitos

A especificação deste módulo está contida no arquivo SPEC.md.


## Copiar o repositório local

git clone https://bitbucket.org/rjchagas/product-ms.git


## Compilar e rodar o microserviço

./mvnw clean spring-boot:run

## Banco de dados

O banco de dados pode ser acessado no endereço http://localhost:9999/h2-console/ 

Usuário: abc

Senha: 123

## Acesso ao microserviço

O acesso é feito em localhost e porta 9999. Por exemplo, para listar todos os produtos cadastrados, utilize: 

http://localhost:9999/products


## Exemplos de chamadas aos endpoints


### Terminal 
- todos produtos

curl --location --request GET 'http://localhost:9999/products/'

- produto por id

curl --location --request GET 'http://localhost:9999/products/3'

- pesquisa com filtro

curl --location --request GET 'http://localhost:9999/products/search?min_price=20&max_price=100&q=ote'

- novo produto

curl --location --request POST 'http://localhost:9999/products/' 
--header 'Content-Type: application/json' 
--data-raw '{
    "name": "Computador",
    "description": "Notebook Intel",
    "price": 20
}'


- atualiza produto

curl --location --request PUT 'http://localhost:9999/products/1' 
--header 'Content-Type: application/json' 
--data-raw '{
    "name": "Impressora", 
    "description": "Nova HP Deskjet",
    "price": 2.00
}'

- exclui produto por id

curl --location --request DELETE 'http://localhost:9999/products/2'


### Postman

O arquivo products.postman_collection.json contém alguns exemplos das chamadas aos endpoints. Ele pode ser importado pelo aplicativo Postman.
