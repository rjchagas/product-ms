package com.compassouol.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compassouol.product.model.Product;
import com.compassouol.product.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@PostMapping("/")
	Product createProduct(@Valid @RequestBody Product product) {
		return productService.createProduct(product);
	}

	@GetMapping("/{id}")
	Product readProduct(@PathVariable String id) {
		return productService.readProduct(id);
	}
	
	@PutMapping("/{id}")
	Product updateProduct(@Valid @RequestBody Product product, @PathVariable String id) {
		return productService.updateProduct(product, id);
	}

	@DeleteMapping("/{id}")
	void deleteProduct(@PathVariable String id) {
		productService.deleteProduct(id);
	}

	@GetMapping("/")
	List<Product> findAll() {
		return productService.findAll();
	}

	@GetMapping("/search")
	List<Product> findAllApplyingFilter(@RequestParam(value="min_price", defaultValue="0") double minPrice, 
			@RequestParam(value="max_price", defaultValue="1e10") double maxPrice, 
			@RequestParam(value="q", defaultValue="") String q) {
		return productService.findAllApplyingFilter(minPrice, maxPrice, q);
	}
	
}
