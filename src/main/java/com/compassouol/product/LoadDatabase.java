package com.compassouol.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.compassouol.product.model.Product;
import com.compassouol.product.repository.ProductRepository;

@Configuration
class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	CommandLineRunner initDatabase(ProductRepository productRepository) throws Exception {

		productRepository.deleteAll();

		return args -> {
			log.info("Preloading " + productRepository.save(new Product("Computador", "Notebook Samsung", 200.50)));
			log.info("Preloading " + productRepository.save(new Product("Impressora HP", "HP Deskjet 2330", 70.50)));
			log.info("Preloading " + productRepository.save(new Product("A Tour of C++", "Livro de Bjarne Stroustrup", 70.50)));
			log.info("Preloading " + productRepository.save(new Product("Spring in Action", "Livro de Craig Walls", 70.50)));
		};

	}

}