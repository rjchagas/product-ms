package com.compassouol.product.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compassouol.product.exception.ItemNotFoundException;
import com.compassouol.product.model.Product;
import com.compassouol.product.repository.ProductRepository;

@Component
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public Product createProduct(Product product) {
		return productRepository.save(product);		
	}
	
	public Product readProduct(String id) {
		return productRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
	}
	
	public Product updateProduct(Product product, String id) {
		return productRepository.findById(id)
				.map(updatedProduct -> {
					updatedProduct.setName(product.getName());
					updatedProduct.setDescription(product.getDescription());
					updatedProduct.setPrice(product.getPrice());
					return productRepository.save(updatedProduct);
				})
				.orElseThrow(() -> new ItemNotFoundException(id));
	}
	
	public void deleteProduct(String id) {
		if (productRepository.existsById(id) ) {
			productRepository.deleteById(id);
		} else throw new ItemNotFoundException(id);
	}

	public List<Product> findAll() {
		var it = productRepository.findAll();
		var products = new ArrayList<Product>();
		it.forEach(e -> products.add(e));
		return products;
	}
	
	public List<Product> findAllApplyingFilter(double minPrice, double maxPrice, String q) {
		return productRepository.findAllApplyingFilter(minPrice, maxPrice, q);
	}
	
}
