package com.compassouol.product.service;

import java.util.List;

import com.compassouol.product.model.Product;

public interface ProductService {

	Product createProduct(Product product);
	
	Product readProduct(String id);
	
	Product updateProduct(Product product, String id);
	
	void deleteProduct(String id);

	List<Product> findAll();

	List<Product> findAllApplyingFilter(double minPrice, double maxPrice, String q);

}
