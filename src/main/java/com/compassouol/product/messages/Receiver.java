package com.compassouol.product.messages;

import java.util.concurrent.CountDownLatch;

import org.springframework.stereotype.Component;

@Component
public class Receiver {

	CountDownLatch count = new CountDownLatch(1);
	
	public void receiveMessage(String message) {
		System.out.println("Recebida msg: <" + message + ">");
		count.countDown();
	}

	public CountDownLatch getCount() {
		return count;
	}
}
