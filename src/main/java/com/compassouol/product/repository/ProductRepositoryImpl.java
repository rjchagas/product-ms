package com.compassouol.product.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.compassouol.product.model.Product;

@Component
public class ProductRepositoryImpl {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	public List<Product> findAllApplyingFilter(double minPrice, double maxPrice, String q) {
		
		final List<Criteria> criterias = new ArrayList<>();
		
		if (minPrice > 0 || maxPrice < 1e10) {
			Criteria c1 = Criteria.where("price").gte(minPrice);
			Criteria c2 = Criteria.where("price").lte(maxPrice);
			if (maxPrice == 1e10)
				criterias.add(c1); 
			else if (minPrice == 0)
				criterias.add(c2);
			else 
				criterias.add(c1.andOperator(c2));
		}
		
		if (!q.isEmpty()) {
			Criteria c1 = Criteria.where("name").regex(q);
			Criteria c2 = Criteria.where("description").regex(q);
			criterias.add(new Criteria().orOperator(c1, c2));
		}
		
		final Query query = new Query();
		criterias.stream().forEach(c -> query.addCriteria(c));
	
		return mongoTemplate.find(query, Product.class);
	}
	
}
