package com.compassouol.product.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.compassouol.product.model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {

	public List<Product> findAllApplyingFilter(double minPrice, double maxPrice, String q);
	
}

